import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import multiMonthPlugin from '@fullcalendar/multimonth'
import bootstrap5Plugin from '@fullcalendar/bootstrap5';
import iCalendarPlugin from '@fullcalendar/icalendar';
import allLocales from '@fullcalendar/core/locales-all';
import { default as Modal } from 'js/bootstrap/src/modal';


let calendar: Calendar;
const modal = new Modal("#exampleModal", {});

document.addEventListener('DOMContentLoaded', () => {
  const calendarEl = document.getElementById('calendar')!;

  calendar = new Calendar(calendarEl, {
    plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin, multiMonthPlugin, bootstrap5Plugin, iCalendarPlugin],
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'multiMonthYear,dayGridMonth,timeGridWeek,timeGridDay,listMonth'
    },
    events: {
      url: '{{ $.Site.Params.fsCalendar }}',
      format: 'ics'
    },
    locales: allLocales,
    locale: '{{ $.Site.Language.Lang }}',
    buttonText: {
      today:    '{{ i18n "today" . }}',
      year:     '{{ i18n "year" . }}',
      month:    '{{ i18n "month" . }}',
      week:     '{{ i18n "week" . }}',
      day:      '{{ i18n "day" . }}',
      list:     '{{ i18n "list" . }}',
      prev:     '‹',
      next:     '›'
    },
    buttonIcons: false,
    initialView: window.matchMedia('(max-width: 500px)').matches ? 'timeGridDay' : 'timeGridWeek',
    eventClick(info) {
      info.jsEvent.preventDefault() // don't let the browser navigate

      let title = info.event._def.title
      let description = info.event._def.allDay ? "<span class=\"badge rounded-pill text-bg-primary\">{{ i18n "calendar-allday" }}</span>" : ""
      description += "<table class='table'><tr><td>{{ i18n "calendar-from" }}</td><td>" + info.event._instance.range.start.toLocaleDateString(info.event._context.options.locale , { timeZone: "UTC" }) + " " + info.event._instance.range.start.toLocaleTimeString(info.event._context.options.locale , { timeZone: "UTC" }) + "</td></tr>"
      description += "<tr><td>{{ i18n "calendar-until" }}</td><td>"+ info.event._instance.range.end.toLocaleDateString(info.event._context.options.locale , { timeZone: "UTC" }) + " " + info.event._instance.range.end.toLocaleTimeString(info.event._context.options.locale , { timeZone: "UTC" }) + "</td></tr>"
      if(info.event._def.extendedProps.description)  description += "<tr><td>{{ i18n "calendar-description" }}</td><td>" + info.event._def.extendedProps.description + "</td></tr>";
      if(info.event._def.url) description += "<tr><td>URL</td><td><a href='" + info.event._def.url + "' target='_blank'>" + info.event._def.url + "</a></td></tr>"
      if (info.event._def.extendedProps.location) description += "<tr><td>{{ i18n "calendar-location" }}</td><td>" + info.event._def.extendedProps.location + "</td></tr>"
      description += "</table>"
      let modalElement = document.getElementById('exampleModal');
      let modalTitle = modalElement?.querySelector('.modal-title') as HTMLElement;
      modalTitle.innerText = title;
      let modalBody = modalElement?.querySelector('.modal-body') as HTMLElement;
      modalBody.innerHTML = description;
      modal.show();
      },
    themeSystem: 'bootstrap5',
    navLinks: true,
    dayMaxEvents: true,
  });

  calendar.render();

  document.querySelectorAll('.fc-toolbar.fc-header-toolbar').forEach(element => {
    element.classList.add('row');
  });

  document.querySelectorAll('.fc-toolbar-chunk').forEach(element => {
    element.classList.add('col-lg-4');
  });

  document.querySelectorAll('.fc-button').forEach(element => {
    element.classList.add('row');
    element.classList.add('col-lg-12');
  });
});
